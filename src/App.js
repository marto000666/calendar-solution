import { useState } from 'react';
import './App.css';
import dataArr from './calendar.json';
import Calendar from './components/Calendar';
import Reservation from './components/Reservation';



function App() {
  const [ data, setData ] = useState(dataArr)

  return (
    <div className="">
      <Reservation data={data} setData={setData}/>
      <div className="mt-20 m-auto grid grid-cols-5 w-3/5">
        <div className="border-r-2 col-span-1">
          <h1 className="pt-20 text-xl text-bold">Room price</h1> 
          <h1 className="pt-16 text-xl text-bold">Room status</h1> 
        </div>
        <div className="flex overflow-x-scroll col-span-4">
          {data.map(el => <Calendar date={el.date} status={el.status} price={el.price}/>)}
        </div>
      </div>
    </div>
  );
}

export default App;
