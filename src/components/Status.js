const Status = ({ status }) => {
  return (
    <div>
      {(status === 'blocked') 
        ? <div className="w-28 bg-red-300 h-24 pt-3 m-0 pr-0 hover:bg-red-500" />
        : <div className="w-28 bg-green-300 h-24 pt-3 hover:bg-green-500" />
      }
    </div>
  )
};

export default Status;