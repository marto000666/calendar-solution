import RoomPrice from "./RoomPrice";
import Status from "./Status";

const days = {
  0: 'Sun',
  1: 'Mon',
  2: 'Tue',
  3: 'Wed',
  4: 'Thu',
  5: 'Fri',
  6: 'Sat',
}

const Calendar = ({ date, status, price }) => {
  const dateObj = new Date(date);
  const dateNumber = dateObj.getUTCDate();
  const month = dateObj.toLocaleString('en-us', { month: 'long' });
  const day = days[dateObj.getDay()]

  return (
    <div>
      <div className="border-r-2 text-right">
        <div className="w-28 ">
        <h1 className="text-lg pr-2 float-left text-gray-500">{month}</h1>
        {(day === 'Sat' || day === 'Sun') 
          ? <h1 className="text-lg p-0 font-bold pr-2">{day}</h1>
          : <h1 className="text-lg pr-2">{day}</h1>
        }
        {(day === 'Sat' || day === 'Sun') 
          ? <h1 className="text-lg font-bold pr-2">{dateNumber}</h1>
          : <h1 className="text-lg pr-2">{dateNumber}</h1>
        }
        </div>
      </div>
      <Status status={status} />
      <RoomPrice price={price} />
    </div>
  )
};

export default Calendar;