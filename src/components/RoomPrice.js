const RoomPrice = ({ price }) => {
  return (
    <div className="text-right pt-3 pb-3 group">
      <h1 className="text-xl bg-red-100 border-r-4 border-white pr-3 group-hover:bg-red-500" >US$</h1>
      <h1 className="text-xl bg-red-100 border-r-4 border-white pr-3 group-hover:bg-red-500">{price}</h1>
    </div>
  )
};

export default RoomPrice;