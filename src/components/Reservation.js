import { useState } from "react";
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import PopUpAlert from "./PopUpAlert";


const Reservation = ({ data, setData }) => {
  const [open, setOpen] = useState(false);
  const [guest, setGuest] = useState('');
  const [arriving, setArriving] = useState(new Date('2021-06-11').toISOString().substr(0, 10));
  const [leaving, setLeaving] = useState(new Date('2021-06-12').toISOString().substr(0, 10));
  const [reservation, setReservation] = useState([])
  const [notify, setNotify] = useState({
    isOpen: false,
    message: "",
    type: "",
  });

  const handleCloseNotification = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setNotify({
      isOpen: false,
    });
  };

  const handleGuest = (e) => {
    setGuest(e.target.value);
    console.log(guest);
  }

  const reserve = () => {
    let date1 = new Date(arriving);
    let date2 = new Date(leaving);
    // check for owner
    if (guest.length === 0) {
      setNotify({
        isOpen: true,
        message: "Invalid Owner",
        type: "error",
      });
      return;
    }

    // first check for leaving > arriving
    if (date2.getTime() < date1.getTime()) {
      setNotify({
        isOpen: true,
        message: "Invalid date",
        type: "error",
      });
      setOpen(true)
      return;
    }
    // check for arriving date and dates in arr
    if (  date1.getTime() < new Date(data[0].date).getTime() || 
          date1.getTime() > new Date(data[data.length-1].date).getTime() ) {
      setNotify({
        isOpen: true,
        message: "Invalid date",
        type: "error",
      });
      return;
    }
    // check for leaving date and dates in arr
    if (  date2.getTime() < new Date(data[0].date).getTime() || 
          date2.getTime() > new Date(data[data.length-1].date).getTime() ) {
      setNotify({
        isOpen: true,
        message: "Invalid date",
        type: "error",
      });
      return;
    }
    // check for available room
    date1 = date1.toISOString().substr(0, 10);
    date2 = date2.toISOString().substr(0, 10);
    const arriveIndex = data.findIndex((el) => el.date === date1);
    const leaveIndex = data.findIndex((el) => el.date === date2);
    const TravelDurationArr = [...data].filter((_,index) => index >= arriveIndex && index < leaveIndex )
    if (TravelDurationArr.map(el => el.status).some(status => status !== 'bookable')) {
      setNotify({
        isOpen: true,
        message: "The room can not be rented for these days",
        type: "error",
      });
      return;
    }

    setData(prev => {
      const arr = prev.map((el, index) => {
        if (index >= arriveIndex && index <= leaveIndex) {
          el.status = 'blocked'
        }
        return el;
      })
      return arr
    })
    setReservation(prev => {
      const reservation = {
        id: prev.length + 1,
        startDate: date1,
        endDate: date2,
        guestName: guest
      }
      return [...prev, reservation];
    })
    
    setNotify({
      isOpen: true,
      message: `Successfully rented for the days between ${date1} and ${date2}!\nThank you, ${guest}!`,
      type: "success",
    });
    setOpen(false);
    setGuest('');
  }

  return (
    <div className="mt-12 text-center">
      <PopUpAlert
            notify={notify}
            handleCloseNotification={handleCloseNotification}
      />
      <button 
        className="
          text-xl rounded-full py-3 px-6 
        border-red-100 border-2 bg-red-100 
        hover:bg-red-500"
        onClick={() => setOpen(true)}
      >
        Reserve
      </button>
      <Dialog 
        open={open} 
        onClose={() => setOpen(false)}
      >
        <h1 className="text-2xl pt-2 pl-3 font-bold"> Make a reservation </h1>
        <h1 className="text-xl pt-2 pl-3 pr-3">
          To successfully finish the reservation please fill out the forms.
        </h1>
        <form className="pt-3 ">
          <label className="text-xl ml-3">Owner:</label>
          <input
            type="text"
            name="name"
            autocomplete="off"
            className="border-b-2 border-black outline-none ml-2 w-3/5 text-xl"
            onChange={(e) => handleGuest(e)}
          />
        </form>
        
        <div className="grid grid-cols-2 m-auto gap-6 pt-5">
          <form noValidate>
            <TextField
              label="Arriving"
              type="date"
              defaultValue={arriving}
              onChange={(e) => setArriving(e.target.value)}
            />
          </form>
          <form noValidate>
            <TextField
              label="Leaving"
              type="date"
              defaultValue={leaving}
              onChange={(e) => setLeaving(e.target.value)}
            />
          </form>
        </div>
       
        <DialogActions>
          <button 
            className="text-xl py-3 px-6 border-red-200 border-2 bg-red-200 hover:bg-red-500"
            onClick={() => setOpen(false)}
          >
            Cancel
          </button>
          <button 
            className="text-xl py-3 px-6 border-green-200 border-2 bg-green-200 hover:bg-green-500"
            onClick={() => reserve()}
          >
            Reserve
          </button>
        </DialogActions>
      </Dialog>
    </div>
  )
};

export default Reservation;